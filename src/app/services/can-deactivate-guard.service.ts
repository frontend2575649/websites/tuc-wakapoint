import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { EditCustomerComponent } from '../edit-customer/edit-customer.component';

@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuard implements CanDeactivate<EditCustomerComponent> {
  canDeactivate(component: EditCustomerComponent): boolean {
    return true;
  }
}
