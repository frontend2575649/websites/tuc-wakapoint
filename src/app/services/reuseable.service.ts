import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, catchError, throwError } from 'rxjs';


let headers = new HttpHeaders({
  'Content-Type': 'application/json',
  'Authorization': 'YmQ1NzMxYTMyOWFiNGZkZjlhYzYyZDQ5ZThlZWQzYTM='
});
let options = { headers: headers };

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    Authorization: 'YmQ1NzMxYTMyOWFiNGZkZjlhYzYyZDQ5ZThlZWQzYTM='
  })
};

@Injectable({
  providedIn: 'root'
})
export class ReuseableService {
  baseURL = environment.apiURL;
  profileCode = '';

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  register(model: any) {
    return this.http.post(this.baseURL + 'register', model, httpOptions)
      .pipe(catchError(this.handleError))
  }

  checkBalance(model: any) {
    return this.http.post(this.baseURL + 'getbalance', model, httpOptions)
      .pipe(catchError(this.handleError))
  }

  getAccount(model: any) {
    return this.http.post(this.baseURL + 'getaccount', model, httpOptions)
      .pipe(catchError(this.handleError))
  }

  updateAccount(model: any) {
    return this.http.post(this.baseURL + 'updateaccount', model, httpOptions)
      .pipe(catchError(this.handleError))
  }
}
