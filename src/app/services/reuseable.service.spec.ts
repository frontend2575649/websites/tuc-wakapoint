import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing"
import { ReuseableService } from './reuseable.service';

fdescribe('ReuseableService', () => {
  let reuseableService: ReuseableService,
    httpTestingController: HttpTestingController;
  // let baseURL = 'https://connect.thankucash.com/api/opcon/account/';
  let baseURL = 'https://localhost:5002/api/opcon/account/';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReuseableService],
      imports: [HttpClientTestingModule]
    });
    reuseableService = TestBed.get(ReuseableService);
    httpTestingController = TestBed.get(HttpTestingController)
  });

  it('#reuseableService should be created', () => {
    expect(reuseableService).toBeTruthy();
  });

  it('#checkBalance should check a user\'s balance', () => {
    let model = {
      Pin: "1234",
      accountNumber: "2349009009000"
    }

    reuseableService.checkBalance(model)
      .subscribe((response: any) => {
        expect(response).toBeTruthy('Balance loaded');
      })

    const req = httpTestingController.expectOne(baseURL + 'getbalance');
    expect(req.request.method).toEqual('POST');
    req.flush({
      Message: "Balance loaded",
      ResponseCode: "HCG200",
      Result: { balance: 0 },
      Status: "Success"
    })
  });

  it('#register should create a new user', () => {
    let model = {
      dateOfBirth: "Fri Mar 02 2010 12:54:04 GMT+0100 (West Africa Standard Time)",
      emailAddress: "test@gmail.com",
      firstName: "test",
      gender: "male",
      lastName: "mike",
      mobileNumber: "080292929292"
    }

    reuseableService.register(model)
      .subscribe((response: any) => {
        expect(response).toBeTruthy('Account registration successful');
      })

    const req = httpTestingController.expectOne(baseURL + 'register');
    expect(req.request.method).toEqual('POST');
    req.flush({
      "Status": "Success",
      "Message": "Account registration successful",
      "Result": {
        "accountNumber": "000000000"
      },
      "ResponseCode": "HCG200"
    })
  });

  afterEach(() => {
    httpTestingController.verify();
  })
});
