import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

import { RegisterComponent } from './register.component';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing"
import { ToastrService } from 'ngx-toastr';
import { ReuseableService } from '../services/reuseable.service';
import { Router } from '@angular/router';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


fdescribe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let toastrServiceStub : Partial<ToastrService>,
      routerStub: Partial<Router>,
      reuseableServiceStub: Partial<ReuseableService>;


  // Create a fake TwainService object with a `getQuote()` spy
  const reuseableService = jasmine.createSpyObj('ReuseableService', ['register']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      imports: [
        BrowserDynamicTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        BsDatepickerModule.forRoot()
      ],
      providers: [
        { provide: ToastrService, useValue: toastrServiceStub },
        { provide: Router, useValue: routerStub },
        { provide: ReuseableService, useValue: reuseableServiceStub },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('#register() should register a new user', () => {
  //   const result = reuseableServiceStub.register()
  //       .subscribe(res => console.log('response is'))

  //   // component.register();
  //   console.log('result is ', result)
  //   expect(component).toBeTruthy();
  // });

});
