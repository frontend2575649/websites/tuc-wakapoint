import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ReuseableService } from '../services/reuseable.service';
import { formatDate } from '@angular/common';
import { NotificationService } from '../services/notification.service';
import { Route, Router } from '@angular/router';
import { Gtag } from 'angular-gtag';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  isBusy = false;
  bsConfig: Partial<BsDatepickerConfig>;
  minDate: Date = new Date();
  maxDate: Date = new Date();
  dateOfBirthVal: Date = new Date();
  responseMessageType: string = '';
  responseMessage: string = 'hello';

  registerForm = this.fb.group({
    firstName: ['', [Validators.required, Validators.maxLength(128)]],
    middleName: [null],
    lastName: ['', [Validators.required, Validators.maxLength(128)]],
    emailAddress: ['', [
      Validators.required,
      Validators.email]],
    gender: ['male', [Validators.required]],
    dateOfBirth: [null, Validators.required],
    mobileNumber: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(14)]]
  });

  constructor(
    private _Router: Router,
    private fb: FormBuilder,
    private reuseableService: ReuseableService,
    private notifyService: NotificationService,
    private router: Router,
    private gtag: Gtag
  ) {
    this.bsConfig = {
      containerClass: 'theme-red'
    };
    this.minDate = new Date(this.minDate.setFullYear(this.minDate.getFullYear() - 82));
    this.maxDate = new Date(this.maxDate.setFullYear(this.maxDate.getFullYear() - 13));
  }

  ngOnInit(): void {
    console.log(this.registerForm)
  }

  public ShowEdit = false;
  // public tMobileNumber = '';
  _BalanceRequest =
    {
      accountNumber: "",
      pin: "",
    }
  register() {
    this.ShowEdit = false;
    this.isBusy = true;
    this.gtag.event('register')
    let model = this.registerForm.value;
    this._BalanceRequest.accountNumber = model.mobileNumber
    console.log(model)
    let _Request = window.btoa(JSON.stringify(model));
    let _R = {
      request: _Request
    };
    this.reuseableService.register(_R).subscribe(
      (res: any) => {

        // this.responseMessageType = res.Status;
        // this.notifyService.showSuccess(res.Message);
        // this.responseMessage = res.Message;
        // this.registerForm.reset();
        // this.isBusy = false;

        if (res.Status == 'Success') {
          // this.notifyService.showSuccess(res.Message);
          // this.userDetail = res.Result;
          // this.reuseableService.profileCode = this.userDetail.profileCode;
          // this.balanceResult = Number(Math.round(res.Result.balance / 100).toFixed(2).toString());
          // if (this.balanceResult == NaN) {
          //   this.balanceResult = 0;
          // }

          this.responseMessageType = res.Status;
          this.notifyService.showSuccess(res.Message);
          this.responseMessage = res.Message;
          this.registerForm.reset();
          this.isBusy = false;

        } else {
          // this._ErrorMessages = res.Message;
          // this.responseMessage = res.Message;
          // this.notifyService.showError(res.Message);
          if (res.ResponseCode == 'CACUST1423' || res.ResponseCode == 'CACUST1424') {
            this.ShowEdit = true;
          }
          else {
            this.ShowEdit = false;
          }
          this.responseMessageType = res.Status;
          this.notifyService.showSuccess(res.Message);
          this.responseMessage = res.Message;
          this.registerForm.reset();
          this.isBusy = false;
        }


      },
      (error: any) => {
        this.notifyService.showError(error);
        this.responseMessage = error.Message;
        this.isBusy = false;
      });
  }

  // checkWakaPointForm : any ;
  // userDetail : any ={};
  checkBalance() {
    if (this._BalanceRequest.pin == undefined || this._BalanceRequest.pin == null || this._BalanceRequest.pin == "") {
      this.notifyService.showError("Please enter your redeem pin to update profile");
    }
    else {
      // this.checkButtonBusy = true;
      let _Request = window.btoa(JSON.stringify(this._BalanceRequest));
      let _R = {
        request: _Request
      };
      // this._ErrorMessages = "";
      this.reuseableService.checkBalance(_R).subscribe(
        (res: any) => {
          if (res.Status == 'Success') {
            this.reuseableService.profileCode = res.Result.profileCode;
            this._Router.navigate(['/updateprofile']);
          } else {
            this.notifyService.showError(res.Message);
          }
        },
        (error: any) => {
          this.notifyService.showError(error);
        });
    }

  }
  showOtpError = false;
  onPinChange(pin: string) {
    this._BalanceRequest.pin = pin;
    if (pin.length != 4) {
      this.showOtpError = true;
    } else {
      this.showOtpError = false;
    }
  }
}
