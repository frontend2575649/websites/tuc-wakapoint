import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ReactiveFormsModule, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Gtag } from 'angular-gtag';
import { Observable } from 'rxjs';
import { NotificationService } from '../services/notification.service';
import { ReuseableService } from '../services/reuseable.service';

@Component({
  selector: 'app-check-waka-points',
  templateUrl: './check-waka-points.component.html',
  styleUrls: ['./check-waka-points.component.css']
})
export class CheckWakaPointsComponent implements OnInit {
  mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";
  balanceResult: number = 0;
  redemPoints = false;
  checkButtonBusy = false;
  pin: string = '';
  showOtpError = false;
  userDetail: any = {};
  responseMessage = '';
  @ViewChild('ngPinInput', { static: false }) ngPinInput: any;


  constructor(
    private fb: FormBuilder,
    private reuseableService: ReuseableService,
    private notifyService: NotificationService,
    private gtag: Gtag) { }

  checkWakaPointForm = this.fb.group({
    accountNumber: ['',
      [Validators.required]],
    Pin: ['', [Validators.required]],
    Token: ['']
  })

  ngOnInit(): void {
  }

  backToHome() {
    this.redemPoints = false;
    this.checkWakaPointForm.reset();
  }
  _ErrorMessages = "";
  wakaPointId:any;
  shownWakapointId:any;
  checkBalance(model: any) {
this.wakaPointId=model.accountNumber;
    this.gtag.event('check_balance')
    this.checkButtonBusy = true;
    let _Request = window.btoa(JSON.stringify(model));
    let _R = {
      request: _Request
    };
    this._ErrorMessages = "";
    this.reuseableService.checkBalance(_R).subscribe(
      (res: any) => {
        this.checkButtonBusy = false;
        if (res.Status == 'Success') {
          // this.shownWakapointId=this.wakaPointId;
          if(res.Result.accountNumber){
            this.shownWakapointId=  res.Result.accountNumber;
          }
         
          this.redemPoints = true;
          this.notifyService.showSuccess(res.Message);
          // this.responseMessage = res.Message;
          this.userDetail = res.Result;
          this.reuseableService.profileCode = this.userDetail.profileCode;
          this.balanceResult = Number(Math.round(res.Result.balance / 100).toFixed(2).toString());
          if (this.balanceResult == NaN) {
            this.balanceResult = 0;
          }
        } else {
          this._ErrorMessages = res.Message;
          this.responseMessage = res.Message;
          this.notifyService.showError(res.Message);
        }
      },
      (error: any) => {
        this.notifyService.showError(error);
        this.responseMessage = error.Message;
        this.checkButtonBusy = false;
      });
  }

  onPinChange(pin: string) {
    this.pin = pin;
    this.checkWakaPointForm.patchValue({
      Pin: pin
    })
    if (pin.length != 4) {
      this.showOtpError = true;
    } else {
      this.showOtpError = false;
    }
  }

}
