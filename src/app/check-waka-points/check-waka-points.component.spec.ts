import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { CheckWakaPointsComponent } from './check-waka-points.component';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing"
import { ToastrService } from 'ngx-toastr';

fdescribe('CheckWakaPointsComponent', () => {
  let component: CheckWakaPointsComponent;
  let fixture: ComponentFixture<CheckWakaPointsComponent>;
  let toastrServiceStub : Partial<ToastrService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckWakaPointsComponent ],
      imports: [ ReactiveFormsModule, HttpClientTestingModule  ],
      providers: [
        { provide: ToastrService, useValue: toastrServiceStub }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckWakaPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
