import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CheckWakaPointsComponent } from './check-waka-points/check-waka-points.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GtagModule } from 'angular-gtag';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ToastrModule } from 'ngx-toastr';
import { NgOtpInputModule } from 'ng-otp-input';
import { BlockCopyPasteDirective } from './block-copy-paste.directive';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';
import { TermsComponent } from './terms/terms.component';

const routes: Routes = [
  // { path: '', pathMatch: 'full', component: CheckWakaPointsComponent },
  { path: 'balancecheck', pathMatch: 'full', component: CheckWakaPointsComponent },
  { path: '', pathMatch: 'full', component: RegisterComponent },
  { path: 'register', pathMatch: 'full', component: RegisterComponent },
  { path: 'updateprofile', pathMatch: 'full', component: EditCustomerComponent, canDeactivate: [CanDeactivateGuard] },
  // { path: 'updateprofile', pathMatch: 'full', component: EditCustomerComponent },
  { path: 'updateprofile/:referencekey', pathMatch: 'full', component: EditCustomerComponent },
  { path: 'profile/:referencekey', pathMatch: 'full', component: EditCustomerComponent },
  { path: 'howitworks', pathMatch: 'full', component: HowItWorksComponent },
  { path: 'terms', pathMatch: 'full', component: TermsComponent },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HowItWorksComponent,
    CheckWakaPointsComponent,
    BlockCopyPasteDirective,
    EditCustomerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgOtpInputModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule, //this is the module for form incase form validation
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    RouterModule.forRoot(routes),
    GtagModule.forRoot({ trackingId: 'G-SRL4CP9G6H', trackPageviews: true })
  ],
  exports: [RouterModule],
  providers: [CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
