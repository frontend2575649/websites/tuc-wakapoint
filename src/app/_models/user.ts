export interface User {
    firstName?: string;
    middleName?: string;
    lastName?: string;
    emailAddress?: string;
    gender?: string;
    dateOfBirth?: Date;
    mobileNumber?: string;
    pin?: string;
}