import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Gtag } from 'angular-gtag';
import { NotificationService } from '../services/notification.service';
import { ReuseableService } from '../services/reuseable.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { User } from '../_models/user';
import { browserRefresh } from '../app.component';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  user!: User;
  userData = {
    dateOfBirth: new Date()
  };
  minDate: Date = new Date();
  maxDate: Date = new Date();
  busy = false;
  showOtpError = false;
  pin: string = '';
  responseMessage: string = '';
  browserRefreshVal = false;

  @ViewChild('editForm', { static: false }) editForm!: NgForm;
  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    event.returnValue = false;
    this.browserRefreshVal = event.returnValue
  }


  constructor(
    private fb: FormBuilder,
    private reuseableService: ReuseableService,
    private notifyService: NotificationService,
    private router: Router,
    private gtag: Gtag,
    private _ActivatedRoute: ActivatedRoute
  ) {
    this.minDate = new Date(this.minDate.setFullYear(this.minDate.getFullYear() - 82));
    this.maxDate = new Date(this.maxDate.setFullYear(this.maxDate.getFullYear() - 13));
  }


  ngOnInit(): void {
    // this.browserRefreshVal = browserRefresh;
    // if(browserRefresh) {
    //   this.router.navigate(['/'])
    // }
    this._ActivatedRoute.params.subscribe((params: Params) => {
      var reference = params["referencekey"];
      if (reference != undefined && reference != null && reference != '') {
        this.reuseableService.profileCode = reference + "==";

        let _Request = window.btoa(JSON.stringify({ profileCode: this.reuseableService.profileCode }));
        let _R = {
          request: _Request
        };
        this.reuseableService.getAccount(_R)
          .subscribe((res: any) => {
            this.userData = res.Result;
            this.user = res.Result;
            this.user.dateOfBirth = new Date(this.userData.dateOfBirth);

            this.responseMessage = res.Message;
            if (res.status == "Error") {
              this.notifyService.showError(res.Message);
            } else {
              this.notifyService.showSuccess(res.Message);
            }

          })
      }
      else {

        if (this.reuseableService.profileCode != undefined && this.reuseableService.profileCode != null && this.reuseableService.profileCode != "") {
          let _Request = window.btoa(JSON.stringify({ profileCode: this.reuseableService.profileCode }));
          let _R = {
            request: _Request
          };
          this.reuseableService.getAccount(_R)
            .subscribe((res: any) => {
              this.userData = res.Result;
              this.user = res.Result;
              this.user.dateOfBirth = new Date(this.userData.dateOfBirth);
              this.responseMessage = res.Message;
              if (res.status == "Error") {
                this.notifyService.showError(res.Message);
              } else {
                this.notifyService.showSuccess(res.Message);
              }
            })
        }
      }

      // this.GetAccountOverviewLite();
    });

  }


  updateUser() {
    this.busy = true;
    const _Request = {
      ...this.editForm.value,
      profileCode: this.reuseableService.profileCode,
      pin: this.pin
    }
    console.log(_Request)

    let _RequestT = window.btoa(JSON.stringify(_Request));
    let _R = {
      request: _RequestT
    };
    this.reuseableService.updateAccount(_R)
      .subscribe((res: any) => {
        this.notifyService.showInfo(res.Message);
        this.responseMessage = res.Message;
        this.busy = false;
      }, (err: any) => {
        this.responseMessage = err.Message;
        this.busy = false;
        console.log(err);
      })
  }

  onPinChange(pin: string) {
    console.log(pin)
    this.pin = pin;
    if (pin.length != 4) {
      this.showOtpError = true;
    } else {
      this.showOtpError = false;
    }
  }

  canDeactivate() {
    console.log('refresh is', this.browserRefreshVal)
    return !this.browserRefreshVal;
  }


}
