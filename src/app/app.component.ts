import { Component, OnInit } from '@angular/core';
import { Route, Router, NavigationStart, Event as NavigationEvent, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { ReuseableService } from './services/reuseable.service';
import { Gtag, GtagModule } from 'angular-gtag';

export let browserRefresh = false;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'WakaPoints';
  subject = new BehaviorSubject(true);
  routeEvent$: any;
  showBackButton = false;
  showHowitWorks = false;

  showBookTickets = false;

  constructor(
    private reuseableService: ReuseableService,
    private router: Router,
    private route: ActivatedRoute,
    private gtag: Gtag) {
  }

  ngOnInit(): void {
    this.routeEvent$ = this.router.events
      .subscribe(
        (event: NavigationEvent) => {
          if (event instanceof NavigationStart) {
            if (event.url === "/register") {
              this.showBookTickets = true;
              this.showBackButton = true;
              this.showHowitWorks = false;
              // this.router.navigate(['/register'])
            } else if (event.url === "/howitworks") {
              this.showBackButton = true;
              this.showHowitWorks = true;
              // this.router.navigate(['/howitworks'])
            } else if (event.url === "/") {
              this.showBookTickets = false;
              this.showBackButton = false
              this.showHowitWorks = false;
            }
            this.gtag.pageview();
            browserRefresh = !this.router.navigated;
          }
        });
  }

  backToHome() {
    this.showBookTickets = false;
    this.showHowitWorks = false;
  }
}
